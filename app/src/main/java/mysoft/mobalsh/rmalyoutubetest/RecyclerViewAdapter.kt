package mysoft.mobalsh.rmalyoutubetest

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mysoft.mobalsh.rmalyoutubetest.RecyclerViewAdapter.MyViewHolder

class RecyclerViewAdapter(private val videos: ArrayList<Video>, private val activity: Activity) :
    RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
    )

    override fun onBindViewHolder(myHolder: MyViewHolder, position: Int) {
        myHolder.txtName.text = videos[position].getvName()

        myHolder.mView.setOnClickListener {
            val vKey = videos[position].getvKey()
            val vName = videos[position].getvName()

            val mapIntent = Intent(activity, YoutubeViewerActivity::class.java)
            mapIntent.putExtra("current_video_key", vKey)
            mapIntent.putExtra("current_video_name", vName)
            activity.startActivity(mapIntent)
        }
    }

    inner class MyViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val txtName: TextView = mView.findViewById(R.id.txtName)
    }

    override fun getItemCount() = videos.size
}