package mysoft.mobalsh.rmalyoutubetest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ListsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lists)

        //Tabs
        val pageTabPager = findViewById<ViewPager2>(R.id.pageTabPager)
        val mainTabsPagerAdabter = TabsPagerAdapter(this)
        pageTabPager.adapter = mainTabsPagerAdabter

        val mainTabLayout = findViewById<TabLayout>(R.id.tabMainTabs)
        TabLayoutMediator(mainTabLayout, pageTabPager) { tab: TabLayout.Tab, position: Int ->
            when (position) {
                0 -> tab.setText("Amir Mounir")

                1 -> tab.setText("Da7ee7")

                else -> tab.setText("Quraan")
            }
        }.attach()
    }
}
