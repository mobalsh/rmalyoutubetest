package mysoft.mobalsh.rmalyoutubetest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class QuraanFragment : Fragment() {
    private var videos = arrayListOf<Video>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videos.clear()
        videos.add(Video("سورة الفاتحة | الحصري | تسجيلات الإذاعة المصرية", "iVrjdS-13Qw"))
        videos.add(Video("سورة النصر | الحصري | تسجيلات الإذاعة المصرية", "lb57_NVOKYU"))
        videos.add(Video("سورة الإخلاص | الحصري | تسجيلات الإذاعة المصرية", "J54_J3_vblA"))
        videos.add(Video("سورة الفلق | الحصري | تسجيلات الإذاعة المصرية", "wI62m9-7mi0"))
        videos.add(Video("سورة الناس | الحصري | تسجيلات الإذاعة المصرية", "AtMGJGVdm_Q"))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mMainView = inflater.inflate(R.layout.fragment_amir_mounir, container, false)
        val recQuraan = mMainView.findViewById<RecyclerView>(R.id.recAmirMounir)
        val layoutManager = LinearLayoutManager(context)
        recQuraan.setLayoutManager(layoutManager)
        recQuraan.setHasFixedSize(true)
        val recyclerViewAdapter = RecyclerViewAdapter(videos, requireActivity())
        recQuraan.setAdapter(recyclerViewAdapter)
        return mMainView
    }
}