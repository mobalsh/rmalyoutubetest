package mysoft.mobalsh.rmalyoutubetest

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

internal class TabsPagerAdapter(f: FragmentActivity) : FragmentStateAdapter(f) {

    private val fragments = arrayOf(AmirMounirFragment(), Da7ee7Fragment(), QuraanFragment())

    override fun createFragment(position: Int) = fragments[position]

    override fun getItemCount() = fragments.size
}
