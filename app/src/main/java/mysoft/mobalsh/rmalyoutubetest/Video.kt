package mysoft.mobalsh.rmalyoutubetest

class Video(private val vName: String, private val vKey: String) {
    fun getvName(): String {
        return vName
    }

    fun getvKey(): String {
        return vKey
    }
}
