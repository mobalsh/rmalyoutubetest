package mysoft.mobalsh.rmalyoutubetest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AmirMounirFragment : Fragment() {
    private var videos = arrayListOf<Video>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videos.clear()
        videos.add(Video("عن الحب والفالنتين | أمير منير | كل يوم", "21MVcEgKWnA"))
        videos.add(
            Video(
                "كل ما أقرب من ربنا حياتي تتعقد أكتر | أمير منير | كل يوم",
                "nqIVc_a_A1A"
            )
        )
        videos.add(Video("اللسان | أمير منير | كل يوم", "L13sayARsaA"))
        videos.add(Video("سب الدين | أمير منير | كل يوم", "4f9mpjYD8IU"))
        videos.add(
            Video(
                "نصيحة في دقيقة هتغير مفهومك عن العبادة | أمير منير | كل يوم",
                "TZbs-j3Sxtg"
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mMainView = inflater.inflate(R.layout.fragment_amir_mounir, container, false)
        val recAmirMounir = mMainView.findViewById<RecyclerView>(R.id.recAmirMounir)
        val layoutManager = LinearLayoutManager(context)
        recAmirMounir.setLayoutManager(layoutManager)
        recAmirMounir.setHasFixedSize(true)
        val recyclerViewAdapter = RecyclerViewAdapter(videos, requireActivity())
        recAmirMounir.setAdapter(recyclerViewAdapter)
        return mMainView
    }
}