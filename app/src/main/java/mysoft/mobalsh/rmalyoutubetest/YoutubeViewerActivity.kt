package mysoft.mobalsh.rmalyoutubetest

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

class YoutubeViewerActivity : YouTubeBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_viewer)

        val currentVideoKey = intent.getStringExtra("current_video_key")
        val currentVideoName = intent.getStringExtra("current_video_name")
        val txtVidNAme = findViewById<TextView>(R.id.txtVidName)

        val mYouTubePlayerView = findViewById<YouTubePlayerView>(R.id.youtube_view)
        val mOnInitializedListener: YouTubePlayer.OnInitializedListener =
            object : YouTubePlayer.OnInitializedListener {
                override fun onInitializationSuccess(
                    provider: YouTubePlayer.Provider,
                    youTubePlayer: YouTubePlayer,
                    b: Boolean
                ) {
                    Log.d(TAG, "youtubeInitializing:Succeeded")
                    txtVidNAme.text = currentVideoName
                    youTubePlayer.loadVideo(currentVideoKey)
                    //youTubePlayer.loadVideos(List of Strings);
                    //youTubePlayer.loadPlayList(String key of youtube play list);
                }

                override fun onInitializationFailure(
                    provider: YouTubePlayer.Provider,
                    youTubeInitializationResult: YouTubeInitializationResult
                ) {
                    youTubeInitializationResult.getErrorDialog(this@YoutubeViewerActivity, 1).show()
                    Log.d(
                        TAG,
                        "youtubeInitializing:Failed as $youTubeInitializationResult"
                    )
                }
            }

        Log.d(TAG, "youtubeInitializing:Started")
        mYouTubePlayerView.initialize(YoutubeConfig.YOUTUBE_API_KEY, mOnInitializedListener)
    }

    companion object {
        private const val TAG = "YoutubeViewerLOGTAG"
    }
}