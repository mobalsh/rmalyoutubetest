package mysoft.mobalsh.rmalyoutubetest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Da7ee7Fragment : Fragment() {
    private var videos = arrayListOf<Video>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videos.clear()
        videos.add(Video("هل الحيوانات تحلم؟ | الدحيح", "T7kFMRhz0Tw"))
        videos.add(Video("النرجسية | الدحيح", "iLyKbfr89Mc"))
        videos.add(Video("نقتل نص الكوكب | الدحيح", "mo-VkD60ZsA"))
        videos.add(Video("صناعة الذكاء | الدحيح", "ujf9RNuBdCU"))
        videos.add(Video("هندسة المصلحة | الدحيح", "D7opPETvi_E"))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mMainView = inflater.inflate(R.layout.fragment_amir_mounir, container, false)
        val recDa7ee7 = mMainView.findViewById<RecyclerView>(R.id.recAmirMounir)
        val layoutManager = LinearLayoutManager(context)
        recDa7ee7.setLayoutManager(layoutManager)
        recDa7ee7.setHasFixedSize(true)
        val recyclerViewAdapter = RecyclerViewAdapter(videos, requireActivity())
        recDa7ee7.setAdapter(recyclerViewAdapter)
        return mMainView
    }
}